/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.model;

import java.util.logging.Logger;

/**
 *
 * @author nayel
 */
public class Simular {
    
    private float monto;
    private int anos; 
     private float calculo;
      private float resultado;

    public float getMonto() {
        return monto;
    }

    public int getAnos() {
        return anos;
    }

    public float getCalculo() {
        calculo=(float)(this.monto*0.1)*this.anos;
       return calculo;
        
        
    }
    
    

    public float getResultado() {
        resultado=this.monto+this.calculo;
        
        return resultado;
        
        
    }


    public void setMonto(float monto) {
        this.monto = monto;
    }

    public void setAnos(int anos) {
        this.anos = anos;
    }

    public void setCalculo(float calculo) {
        this.calculo = calculo;
    }

    public void setResultado(float resultado) {
        this.resultado = resultado;
    }
      
    
   
      
}
